from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'main.html')

def favorites(request):
    return render(request, 'secondPage.html')
