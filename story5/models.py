from django.db import models


# Create your models here.
class Courses(models.Model):
    name = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 100)
    credit = models.IntegerField()
    room = models.CharField(max_length = 50)
    semester = models.CharField(max_length = 9)
    description = models.CharField(max_length = 500, null = True)



    