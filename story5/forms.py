from django import forms

class CoursesForm(forms.Form):

    name = forms.CharField(
        label = 'Course Name',
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'type' : 'text',
                'required': True,
                'autocomplete' : 'off',
            }
        )
    )

    lecturer = forms.CharField(
        label = "Lecturer", 
        max_length = 100,
        widget = forms.TextInput (
            attrs={
                'class':'form-control',
                'type' : 'text',
                'required': True,
                'autocomplete' : 'off',
            }
        )
    )

    credit = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class':'form-control',
                'min':'1',
                'max':'6'
            }
        )
    )

    room = forms.CharField(
        label = "Room", 
        max_length = 50,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'type' : 'text',
                'autocomplete' : 'off',
                'required': True,
            }
        )
    )

    semester = forms.CharField(
        label = "Semester", 
        max_length = 9,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'type' : 'text',
                'autocomplete' : 'off',
                'required': True,
                'placeholder' : 'Year Start / Year End'
            }
        )
    )

    description = forms.CharField(
        label = "Description", 
        max_length = 500,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
                'autocomplete' : 'off',
            }
        )
    )




    


