from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('addCourse/', views.addCourse, name='add'),
    path('', views.courses_show, name = 'courses'),
    path('<int:id>/', views.course_remove, name = 'course_remove'),

]
