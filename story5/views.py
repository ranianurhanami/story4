from django.shortcuts import render,redirect
from .models import Courses
from . import forms
from .forms import CoursesForm


def courses(request) :
    return render(request, 'story5.html')

def addCourse(request):
    formCourse = forms.CoursesForm()
    if request.method == 'POST':
        formInput = forms.CoursesForm(request.POST)
        if formInput.is_valid():
            data = formInput.cleaned_data
            inputData = Courses()
            inputData.name = data['name']
            inputData.lecturer = data['lecturer']
            inputData.credit = data['credit']
            inputData.room = data['room']
            inputData.semester = data['semester']
            inputData.description = data['description']
            inputData.save()
            return redirect('/courses')
        else:
            current_data = Courses.objects.all()
            return render(request, 'addCourse.html',{'form':formCourse, 'status':'failed','data':current_data})
    else:
        current_data = Courses.objects.all()
        return render(request, 'addCourse.html' ,{'form':formCourse,' data':current_data})

def courses_show(request):
    course = Courses.objects.all().order_by('credit').order_by('name')
    response = {
        'course':course, 
    }
    return render(request,'story5.html', response)

def course_remove(request, id):
    Courses.objects.filter(id=id).delete()
    course = Courses.objects.all().order_by('credit').order_by('name')
    response = {
        'course':course, 
    }
    return render(request, 'story5.html', response)

