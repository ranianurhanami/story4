from django.test import TestCase, Client
from django.urls import reverse, resolve;
from .models import Activity, Person
from .forms import ActivityForm, PersonForm
from .views import removeActivity, addActivity, activity, addPerson
from django.apps import apps
from .apps import Story6Config

# Test Model
class Model_Tests(TestCase) :
    def test_model_bisa_tambah_aktivitas_baru(self):
        new_activity = Activity.objects.create(activity='study')
        count_activity = Activity.objects.all().count()
        self.assertEqual(count_activity, 1)

    def test_model_bisa_tambah_orang_baru(self) : 
        new_person = Person.objects.create(person='raniah')
        count_person = Person.objects.all().count()
        self.assertEqual(count_person, 1)

# Test Forms
class Forms_Test(TestCase) :
    def test_apakah_form_activity_valid(self) :
        activity_input = ActivityForm(data = {
            'activity' : 'study'
        })
        self.assertTrue(activity_input.is_valid())

    def test_apakah_form_person_valid(self) :
        person_input = PersonForm(data = {
            'person' : 'raniah'
        })
        self.assertTrue(person_input.is_valid())
    
    def test_validasi_activity_form(self):
        activity_form = ActivityForm(data = {
            'activity': ''
            })
        self.assertFalse(activity_form.is_valid())
        self.assertEqual(
            activity_form.errors['activity'],
            ["This field is required."]
        )
    
    def test_validasi_person_form(self):
        person_form = PersonForm(data = {
            'person': ''
        })
        self.assertFalse(person_form.is_valid())
        self.assertEqual(
            person_form.errors['person'],
            ["This field is required."]
        )

# Test URL
class Urls_Tests(TestCase) :
    def setUp(self):
        self.activity = Activity.objects.create(
            activity="study"
        )
        self.person = Person.objects.create(
            person = "raniah", 
            activity = Activity.objects.get(activity="study")
        )
        self.activities = reverse("story6:activity")
        self.addActivity = reverse("story6:addActivity")
        self.addPerson = reverse("story6:addPerson", args=[self.activity.id])
        self.removeActivity = reverse("story6:removeActivity", args=[self.person.id])

    def test_addActivity_menggunakan_fungsi_yang_benar(self):
        found = resolve(self.addActivity)
        self.assertEqual(found.func, addActivity)

    def test_activity_menggunakan_fungsi_yang_benar(self):
        found = resolve(self.activities)
        self.assertEqual(found.func, activity)

    def test_addPerson_menggunakan_fungsi_yang_benar(self):
        found = resolve(self.addPerson)
        self.assertEqual(found.func, addPerson)

    def test_delete_menggunakan_fungsi_yang_benar(self):
        found = resolve(self.removeActivity)
        self.assertEqual(found.func, removeActivity)

# Test Views and Templates
class Views_and_Templates_Tests(TestCase) :
    def setUp(self):
        self.client = Client()
        self.activities = reverse("story6:activity")
        self.addActivity = reverse("story6:addActivity")
        activity = Activity(activity="study")
        activity.save()

    def test_template_activity_ada(self):
        response = self.client.get(self.activities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activity.html')

    def test_template_addActivity_ada(self):
        response = self.client.get(self.addActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addActivity.html')

    def test_tambah_aktivitas(self) :
        response = self.client.post(self.addActivity,
            {
                'activity': 'study',
            }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_tambah_orang(self):
        response = Client().post('/activity/addPerson/1/', data={
            'person': 'raniah'
        })
        self.assertEqual(response.status_code, 302)

    def test_template_addPerson_ada(self):
        response = self.client.get('/activity/addPerson/1/')
        self.assertTemplateUsed(response, 'addPerson.html')
        self.assertEqual(response.status_code, 200)

    def test_hapus_orang(self):
        activity = Activity(activity='study')
        activity.save()
        person = Person(person='maung', activity=Activity.objects.get(id=1))
        person.save()
        response = self.client.get(reverse('story6:removeActivity', args=[person.id]))
        self.assertEqual(Person.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')
