from django import forms

class ActivityForm(forms.Form):
    
    activity = forms.CharField(
        label = 'Activity Name',
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'type' : 'text',
                'required': True,
                'autocomplete' : 'off',
            }
        )
    )
class PersonForm(forms.Form):
    
    person = forms.CharField(
        label = 'Name',
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'type' : 'text',
                'required': True,
                'autocomplete' : 'off',
            }
        )
    )