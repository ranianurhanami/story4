from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Activity, Person
from .forms import ActivityForm, PersonForm

def activity(request) :
    activity = Activity.objects.all().order_by('activity')
    person = Person.objects.all()
    response = {
        'activity': activity, 
        'person': person,
    }
    return render(request, 'activity.html', response)

def addActivity(request) :
    activityForm = ActivityForm()
    if request.method == "POST":
        inputForm = ActivityForm(request.POST)
        if inputForm.is_valid():
            data = inputForm.cleaned_data
            activityInput = Activity()
            activityInput.activity = data['activity']
            activityInput.save()
            return redirect('/activity/')
        else:
            return render(request, 'addActivity.html', {
                'form': activityForm, 
                'status': 'failed'
            })
    else :
        return render(request, 'addActivity.html', {
            'form': activityForm
        })

def addPerson(request, person_id) :
    personForm = PersonForm()
    if request.method == "POST":
        personInput = PersonForm(request.POST)
        if personInput.is_valid():
            data = personInput.cleaned_data
            newPerson = Person()
            newPerson.person = data['person']
            newPerson.activity = Activity.objects.get(id=person_id)
            newPerson.save()
            return redirect('/activity/')
        else :
            return render(request, 'addPerson.html', {
                'form': personForm, 
                'status': 'failed'
            })
    else:
        return render(request, 'addPerson.html', {
            'form': personForm,
        })

def removeActivity(request, remove_id) :
    activity = Activity.objects.all().order_by('activity')
    person = Person.objects.all()
    deleteUser = Person.objects.get(id=remove_id)
    deleteUser.delete()
    return redirect('/activity/')


