from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.activity, name = 'activity'),
    path('activityForm/', views.addActivity, name='addActivity'),
    path('addPerson/<int:person_id>/', views.addPerson, name='addPerson'),
    path('delete/<int:remove_id>/', views.removeActivity, name = 'removeActivity'),
]
